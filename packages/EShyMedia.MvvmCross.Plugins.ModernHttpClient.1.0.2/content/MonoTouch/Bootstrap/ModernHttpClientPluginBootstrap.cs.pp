using Cirrious.CrossCore.Plugins;

namespace $rootnamespace$.Bootstrap
{
    public class ModernHttpClientPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cheesebaron.MvxPlugins.ModernHttpClient.PluginLoader, Cheesebaron.MvxPlugins.ModernHttpClient.Touch.Plugin>
    {
    }
}