﻿using System;
using Cirrious.CrossCore.Converters;

namespace Discovery2Go.Core.Converters
{
	public class PriceValueConverter : MvxValueConverter<decimal, string>
	{
		protected override string Convert (decimal value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value.ToString("C2");
		}
	}
}

