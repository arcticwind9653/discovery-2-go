﻿using System;
using System.Text.RegularExpressions;
using Cirrious.CrossCore.Converters;

namespace Discovery2Go.Core.Converters
{
	/// <summary>
	/// Converts body text with HTML styling tags to plain text
	/// </summary>
	public class PlainTextValueConverter : MvxValueConverter<string, string>
	{
		private readonly Regex htmlRegex = new Regex("<.*?>");
		protected override string Convert (string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			value = value ?? "";
			return htmlRegex.Replace(value, string.Empty);
		}
	}
}

