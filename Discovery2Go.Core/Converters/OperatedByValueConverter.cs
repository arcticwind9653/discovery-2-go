﻿using System;
using Cirrious.CrossCore.Converters;
using Discovery2Go.Core.Services.Destinations;

namespace Discovery2Go.Core.Converters
{
	public class OperatedByValueConverter : MvxValueConverter<OperatedBy, string>
	{
		protected override string Convert (OperatedBy value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			string imageUrl = string.Empty;
			switch (value)
			{
				case OperatedBy.Both:
				case OperatedBy.CX:
					imageUrl = "res:logo_cx";	//Remember: Do NOT include the .png extension
					break;
				case OperatedBy.DragonAir:
					imageUrl = "res:logo_dragon_air";
					break;
			}
			return imageUrl;
		}
	}
}

