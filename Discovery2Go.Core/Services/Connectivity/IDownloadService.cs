﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Discovery2Go.Core.Services.Connectivity
{
	public interface IDownloadService
	{
		Task<string> DownloadAsString(string url, CancellationTokenSource token = null);
		Task<Stream> DownloadAsStream(string url, CancellationTokenSource token = null);
		Task<byte[]> DownloadAsByteArray(string url, CancellationTokenSource token = null);
		void CancelCurrent();
		void Cancel(CancellationTokenSource token);
	}
}

