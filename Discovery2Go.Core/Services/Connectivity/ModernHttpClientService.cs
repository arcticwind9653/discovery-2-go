﻿using System.Net.Http;
using ModernHttpClient;

namespace Discovery2Go.Core.Services.Connectivity
{
	public class ModernHttpClientService : IModernHttpClientService
	{
		public HttpClient Get ()
		{
			return new HttpClient(GetNativeHandler());
		}

		public HttpClient Get (HttpMessageHandler handler)
		{
			return new HttpClient(handler);
		}

		public HttpMessageHandler GetNativeHandler (bool throwOnCaptiveNetwork = false, bool customSSLVerification = false)
		{
			return new NativeMessageHandler(throwOnCaptiveNetwork, customSSLVerification);
		}
	}
}

