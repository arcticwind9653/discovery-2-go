﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Cheesebaron.MvxPlugins.ModernHttpClient;
using System.Net.Http;

namespace Discovery2Go.Core.Services.Connectivity
{
	public class DownloadService : IDownloadService
	{
		private readonly IModernHttpClientService _modernHttpClientService;
		private CancellationTokenSource _currentToken;

		public DownloadService(IModernHttpClientService modernHttpClientService)
		{
			_modernHttpClientService = modernHttpClientService;
		}

		public async Task<string> DownloadAsString(string url, CancellationTokenSource token = null)
		{
			_currentToken = token ?? new CancellationTokenSource ();

			var client = Init();
			var msg = await client.GetAsync(url, _currentToken.Token);

			if (!msg.IsSuccessStatusCode)
				return null;

			var result = await msg.Content.ReadAsStringAsync ();
			return result;
		}

		public async Task<Stream> DownloadAsStream(string url, CancellationTokenSource token = null)
		{
			_currentToken = token ?? new CancellationTokenSource ();

			var client = Init();
			var msg = await client.GetAsync(url, _currentToken.Token);

			if (!msg.IsSuccessStatusCode)
				return null;

			var result  = await msg.Content.ReadAsStreamAsync();
			return result;
		}

		public async Task<byte[]> DownloadAsByteArray(string url, CancellationTokenSource token = null)
		{
			_currentToken = token ?? new CancellationTokenSource ();

			var client = Init();
			var msg = await client.GetAsync(url, _currentToken.Token);

			if (!msg.IsSuccessStatusCode)
				return null;

			var result = await msg.Content.ReadAsByteArrayAsync();
			return result;
		}

		private HttpClient Init()
		{
			var handler = _modernHttpClientService.GetNativeHandler();
			var outerHandler = new RetryHandler(handler, 3);
			var client = _modernHttpClientService.Get(outerHandler);
			return client;
		}

		public void CancelCurrent ()
		{
			if (_currentToken != null)
				_currentToken.Cancel ();
		}

		public void Cancel (CancellationTokenSource token)
		{
			if (token == null)
				CancelCurrent ();
			else
				token.Cancel();
		}
	}
}

