﻿using System.Net.Http;
using ModernHttpClient;

namespace Discovery2Go.Core.Services.Connectivity
{
	public interface IModernHttpClientService
	{
		HttpClient Get();

		HttpClient Get (HttpMessageHandler handler);

		HttpMessageHandler GetNativeHandler(bool throwOnCaptiveNetwork = false, bool customSSLVerification = false);
	}
}

