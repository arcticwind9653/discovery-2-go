﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.Services.Common
{
	public class Location : MvxNotifyPropertyChanged
	{
		private double _latitude;
		public double Latitude { 
			get { return _latitude; }
			set { _latitude = value;
				RaisePropertyChanged (() => Latitude);
			}
		}

		private double _longitude;
		public double Longitude {
			get { return _longitude; }
			set {
				_longitude = value;
				RaisePropertyChanged (() => Longitude);
			}
		}

		public override bool Equals (object obj)
		{
			var lRhs = obj as Location;
			if (lRhs == null)
				return false;

			return lRhs.Latitude == Latitude && lRhs.Longitude == Longitude;
		}

		public override int GetHashCode()
		{
			return Latitude.GetHashCode() + Longitude.GetHashCode();
		}

		public Location Clone()
		{
			return new Location () {
				Latitude = this.Latitude,
				Longitude = this.Longitude
			};
		}


		public override string ToString ()
		{
			return string.Format("{0:0.00000}, {1:0.00000}", Latitude, Longitude);
		}
	}
}

