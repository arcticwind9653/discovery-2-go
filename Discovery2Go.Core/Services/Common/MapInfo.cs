﻿using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;

namespace Discovery2Go.Core.Services.Common
{
	public class MapInfo : MvxNotifyPropertyChanged
	{
		private Frame _frame;
		public Frame Frame {
			get { return _frame; }
			set {
				_frame = value;
				RaisePropertyChanged (() => Frame);
			}
		}

		private List<Attraction> _markers;
		public List<Attraction> Markers {
			get { return _markers; }
			set { 
				_markers = value;
				RaisePropertyChanged (() => Markers);
			}
		}
	}
}

