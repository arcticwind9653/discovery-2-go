﻿using System;

namespace Discovery2Go.Core.Services.Common
{
	public interface IMapInfoGenesisService
	{
		MapInfo GetMapInfo(string title);
		MapInfo GetMapInfo(Location center);
	}
}

