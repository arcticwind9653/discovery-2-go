﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.Services.Common
{
	public enum AttractionType
	{
		Do,
		Drink,
		Eat,
		Experience,
		See,
		Shop,
		Stay,
		Walk
	}

	public class Attraction : MvxNotifyPropertyChanged
	{
		public Location _location;
		public Location Location {
			get { return _location; }
			set {
				_location = value;
				RaisePropertyChanged(() => Location);
			}
		}

		public AttractionType _attractionType;
		public AttractionType AttractionType {
			get{return _attractionType; }
			set {
				_attractionType = value;
				RaisePropertyChanged(() => AttractionType);
			}
		}

		public string _title;
		public string Title {
			get { return _title; }
			set { _title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		public string _body;
		public string Body {
			get { return _body; }
			set { _body = value;
				RaisePropertyChanged(() => Body);
			}
		}

		public string _image;
		public string Image {
			get { return _image; }
			set { _image = value;
				RaisePropertyChanged(() => Image);
			}
		}

		public string _imageCaption;
		public string ImageCaption {
			get { return _imageCaption; }
			set { _imageCaption = value;
				RaisePropertyChanged(() => ImageCaption);
			}
		}
	}
}

