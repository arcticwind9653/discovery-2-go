﻿using System;
using System.Collections.Generic;
using Cirrious.CrossCore.Platform;
using Cirrious.CrossCore;

namespace Discovery2Go.Core.Services.Common
{
	public class MapInfoGenesisService : IMapInfoGenesisService
	{
		private readonly IMvxJsonConverter _jsonConverter;

		public MapInfoGenesisService (IMvxJsonConverter jsonConverter)
		{
			_jsonConverter = jsonConverter;
		}

		public MapInfo GetMapInfo(string title)
		{
			MapInfo mapInfo;
			string jsonText = null;

			//Mock data of Tokyo first guide: Kappabashi
			if (title == "Discover Tokyo - Kappabashi") {
				jsonText = "{\"frame\":{\"zoomlevel\":14.66447,\"center\":{\"latitude\":35.71098,\"longitude\":139.80042}},\"markers\":[{\"location\":{\"latitude\":35.710849,\"longitude\":139.789901},\"attractiontype\":\"Eat\",\"title\":\"Kappamatsuri\",\"body\":\"Named after the mythical Kappa that has been adopted as a local mascot, this retro restaurant serves <em>okonomiyaki<\\/em> and <em>monja<\\/em>, the omelette-type dishes that come in almost every variety. Once ingredients are chosen, they are cooked by diners at their own tables.\\u000a<p>\u00a0<\\/p>\\u000a<p style=\\\"font-size: 65%;\\\">Photo: <a href=\\\"http:\\/\\/www.flickr.com\\/photos\\/randomidea\\/325537398\\/in\\/photostream\\/\\\">Ryan McBride<\\/a><\\/p>\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23774\\/KAPPABASHI-EAT-KAPPAMATSURI-GENERIC-IMAGE_world-tip.jpg\",\"imageCaption\":\"\"},{\"location\":{\"latitude\":35.716799,\"longitude\":139.789385},\"attractiontype\":\"Drink\",\"title\":\"Kappabashi Coffee\",\"body\":\"To experience a stylish, updated version of a classic Tokyo coffee shop, look no further. There is food available, but it’s all about the lovingly prepared real coffee and teas at Kappabashi Coffee.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23739\\/94120371_world-tip.jpg\",\"imageCaption\":\"Photo: Getty Images\"},{\"location\":{\"latitude\":35.7168024,\"longitude\":139.7971641},\"attractiontype\":\"Shop\",\"title\":\"Sample Shop Maizuru\",\"body\":\"<p>The Kappabashi area has made its name as the place to go for every imaginable catering industry supply, and most famously the plastic models of food displayed outside restaurants in Japan. Along with plastic food, Maizuru also sells novelty items such as funky sushi clocks.<\\/p>\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23742\\/153-BWWN2G_world-tip.jpg\",\"imageCaption\":\"Photo: Alamy\\/Argusphoto\"},{\"location\":{\"latitude\":35.718914,\"longitude\":139.789005},\"attractiontype\":\"Stay\",\"title\":\"Sakura Ryokan\",\"body\":\"One of the only hotels near Kappabashi Street itself, this modern <em>ryokan<\\/em> (inn) offers both Western and Japanese rooms at very reasonable rates. Unlike traditional <em>ryokan<\\/em>, there is no curfew at night, so don’t worry about being locked out.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23745\\/sakura-ryokan-japanese-room-HANDOUT_world-tip.jpg\",\"imageCaption\":\"\"},{\"location\":{\"latitude\":35.710063,\"longitude\":139.8107},\"attractiontype\":\"See\",\"title\":\"Tokyo Sky Tree\",\"body\":\"The latest addition to Tokyo’s high-rise skyline is the tallest tower in the world, a digital broadcast antenna that recently opened with great fanfare. The 634-metre tower is a couple of miles from Kappabashi and is spectacularly lit-up at night.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23751\\/148053058_world-tip.jpg\",\"imageCaption\":\"Photo: Getty Images\"},{\"location\":{\"latitude\":35.7107576,\"longitude\":139.7984776},\"attractiontype\":\"Experience\",\"title\":\"Sumida River Boat Cruises\",\"body\":\"Stroll over to the bridge next to Asakusa Station to catch a relaxing cruise to various locations up and down the Sumida River. Escape from Tokyo’s eternally busy streets to see another side of the city. Audio guides are available in various languages.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23754\\/97540285_world-tip.jpg\",\"imageCaption\":\"Photo: Getty Images\"},{\"location\":{\"latitude\":35.716384,\"longitude\":139.788659},\"attractiontype\":\"Walk\",\"title\":\"Kappabashi Dori (Street)\",\"body\":\"Explore Kappabashi Street to take in the full range of culinary-related tools, equipment and tableware at the shops where Tokyo’s 150,000+ eateries stock up on everything they need, bar the exceptional food.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2013\\/04\\/05\\/23757\\/153-BNF96R_world-tip.jpg\",\"imageCaption\":\"Photo: Alamy\\/Argusphoto\"}]}";
			} else if (title == "Discover Tokyo - Kamakura") {
				jsonText = "{\"frame\":{\"zoomlevel\":13.76271,\"center\":{\"latitude\":\"35.31236\",\"longitude\":\"139.53845\"}},\"markers\":[{\"body\":\"The entranceway has an age-old Japanese style, with shoji sliding paper screens and wooden beams, but the meals combine modernity with old-fashioned Japanese fare. The house specialty is the teuchi buckwheat noodles that are prepared fresh, cut and made by hand.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7581\\/kamakura-01.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Eat\",\"title\":\"Kamakura Matsubara-an\",\"location\":{\"latitude\":\"35.311518\",\"longitude\":\"139.540991\"}},{\"body\":\"This microbrewery is making a big name for itself with a series of tasty European-inspired drinks. The Hayama is close to a traditional English bitter brew, while the Hoshi No Taiga and the Kamakura Star are both tasty pale ales.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7583\\/kamakura-02.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Drink\",\"location\":{\"latitude\":\"35.308985\",\"longitude\":\"139.560727\"},\"title\":\"The Kamakura Beer Brewing Company\"},{\"body\":\"Duck beneath the indigo-dyed noren curtain at the entranceway and paper lovers will be in heaven. Stacked from floor to ceiling are sheets of genuine, handmade washi paper with designs too numerous to count. The store also sells greetings cards, paper fans and other paraphernalia.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7585\\/kamakura-03.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Shop\",\"location\":{\"latitude\":\"35.3220867\",\"longitude\":\"139.5524538\"},\"title\":\"Shato\"},{\"body\":\"Built right above the beach at Shichirigahama, every room in the Prince Hotel has a spectacular view out across Sagami Bay, with the holy island of Enoshima nearby. On clear days, Mount Fuji stands proud on the horizon.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7587\\/kamakura-04.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Stay\",\"location\":{\"latitude\":\"35.30529\",\"longitude\":\"139.515584\"},\"title\":\"Kamakura Prince Hotel\"},{\"body\":\"People have been coming to this shrine since regent Tokiyori Hojo reported that washing coins in the natural spring doubled their material worth. Surrounded on three sides by steep cliffs, the spring is in a cave to the rear of the gorge.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7589\\/kamakura-05.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Do\",\"location\":{\"latitude\":\"35.32575\",\"longitude\":\"139.542248\"},\"title\":\"Wash money at the Zeniarai Benten Shrine\"},{\"body\":\"Demonstrations of this traditional form of archery never fail to impress. Riders must gallop along a 200-metre course, guiding their mounts with their knees and loosing arrows at targets as they pass by. The tradition dates back 32 generations.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7593\\/kamakura-06.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"See\",\"location\":{\"latitude\":\"35.32608\",\"longitude\":\"139.556425\"},\"title\":\"Yabusame (Mounted Archery) at Tsurugaoka Hachimangu Shrine\"},{\"body\":\"Erected in 1252, the Great Buddha of Kamakura has held its steady gaze in the open air since the hall it stood in was destroyed by a 1498 tsunami. For a small fee, visitors can enter the hollow statue.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7595\\/kamakura-07.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Experience\",\"location\":{\"latitude\":\"35.3167\",\"longitude\":\"139.536154\"},\"title\":\"The Great Buddha of Kamakura, Kotoku-in Temple\"},{\"body\":\"To the east of the town, the Gionyama Hiking Trail follows a ridge with impressive views over the sea and heavily wooded surrounding hills. The 45-minute hike is neither long nor strenuous, but offers a nice break from the tourist crowds.\",\"image\":\"http:\\/\\/assets.cougar.nineentertainment.com.au\\/assets\\/CathayPacific\\/2012\\/11\\/14\\/7597\\/kamakura-08.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Walk\",\"location\":{\"latitude\":\"35.315098\",\"longitude\":\"139.554648\"},\"title\":\"Gionyama Hiking Trail\"}]}";
			} else if (title == "Discover Tokyo - Koenji")
				jsonText = "{\"frame\":{\"zoomlevel\":16.72264,\"center\":{\"latitude\":\"35.70497\",\"longitude\":\"139.64999\"}},\"markers\":[{\"body\":\"<p>The alleyways around Koenji Station and even the archways under the tracks bristle with cheerful restaurants serving grilled chicken on wooden skewers. No one really knows why there are so many of these places in Koenji, but it makes it easy to find one you like.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2889/yakitori.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Eat\",\"location\":{\"latitude\":\"35.705385\",\"longitude\":\"139.649867\"},\"title\":\"Yakitori (Grilled Chicken)\"},{\"body\":\"<p>This no-frills, brightly-lit \\\"beer workshop\\\" brews a limited but ever-changing range of craft beers on-site. The pub also offers a small menu that includes fish and chips as well as grilled garlic chicken if you're feeling peckish.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2891/koenji-bakushu.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Drink\",\"location\":{\"latitude\":\"35.7079038\",\"longitude\":\"139.6512169\"},\"title\":\"Koenji Bakushu Kobo \"},{\"body\":\"<p>Koenji is famous for its second-hand clothing shops. Now or Never specialises in menswear ranging from 1940s' to 1960s' Hawaiian shirts all the way to classic American jeans. Leave time to poke through the shop's unclassifiable odds and ends.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2894/now-or-never.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Shop\",\"location\":{\"latitude\":\"35.7034946\",\"longitude\":\"139.649504\"},\"title\":\"Now or Never\"},{\"body\":\"<p>Give the sterile atmosphere of a business hotel a miss and instead, head for the other end of the spectrum at the Koenji Guesthouse. Small, friendly and independently run, it's been described by one visitor as like staying with friends – with all the amenities.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2899/guesthouse.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Stay\",\"location\":{\"latitude\":\"35.7060533\",\"longitude\":\"139.6487253\"},\"title\":\"The Koenji Guesthouse\"},{\"body\":\"<p>Japan's second-largest Awa dance festival, the Koenji Awa Odori attracts about 12,000 dancers and more than a million people to the streets during the last Saturday and Sunday of August. Sing along with the locals during \\\"The Dance of Fools\\\" (\\\"The dancers are fools/The watchers are fools/Both are fools alike so/Why not dance?\\\").</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2901/awa-odori.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Do\",\"location\":{\"latitude\":\"35.7049371\",\"longitude\":\"139.6499542\"},\"title\":\"Awa Odori\"},{\"body\":\"<p>The temple, founded in 1555, is one of 12 temples and shrines in Kōenji. In the 17<sup>th</sup> century it was often visited by the Third Toklugawa Shogun, Tokugawa Iemitsu, and due to his patronage, the entire area was named after the temple. It's a five-minute walk from Koenji Station.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2904/koenji-temple.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"See\",\"location\":{\"latitude\":\"35.7037126\",\"longitude\":\"139.6528048\"},\"title\":\"Shukuhozan Koenji Temple \"},{\"body\":\"<p>Dynamo is a daytime café, skate bar, music venue, skateboard museum and guitar repair shop all in one. It also sells used musical equipment, skateboards and accessories. And there's also a lounge where people can take French or English language lessons from owners Julien Arnaud and Justin Jeske.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/2906/Dynamo.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Experience\",\"location\":{\"latitude\":\"35.705373\",\"longitude\":\"139.647199\"},\"title\":\"Dynamo\"},{\"body\":\"<p>The only way to really take in Koenji is by wandering through its <em>shotengai </em>– shopping streets that meander around the station. While some of the local institutions are mainstays, there is also a constant turnover of new stores to catch the eye and tempt the pocket.</p>\",\"image\":\"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/3145/shotengai.jpg\",\"imageCaption\":\"\",\"attractiontype\":\"Walk\",\"location\":{\"latitude\":\"35.7061346\",\"longitude\":\"139.6495831\"},\"title\":\"Walk around Koenji\"}]}";

			if (String.IsNullOrEmpty (jsonText))
				return null;

			mapInfo = _jsonConverter.DeserializeObject<MapInfo> (jsonText);				

			//TODO: query this from online API

			return mapInfo;
		}

		public MapInfo GetMapInfo(Location center)
		{
			//TODO: using the center query a list of markers
			var markers = new List<Attraction>();

			//TODO: these are Mock data, replace with online search later
			Location bmLocation = new Location () { Latitude = 22.29193, Longitude = 114.20629 };
			Location cxCityLocation = new Location () { Latitude = 22.2980526, Longitude = 113.9341485 };

			if (DistanceBetween (center, bmLocation) < 1.0) { // close to Bauer Media HK
				//load markers close to Bauer Media HK
				Attraction attraction = new Attraction () {
					Location = new Location() {
						Latitude = 22.2925971,
						Longitude = 114.206813
					},
					AttractionType = AttractionType.Do,
					Title = "Ryze Ultimate Trampoline Park",
					Body = "Hong Kong’s first trampoline park has landed in Quarry Bay. Originating from America, Ryze has more than 7,000 square feet of some 40 interconnected trampolines, including angled wall trampolines and launching decks. Test your dunking skills on the SuperSlam and hone your board sports skills by jumping on the “firm-foam” BounceBoards. Enjoy Theme Night on Thursdays and Club Ryze on weekend nights, complete with laser displays. Ryze also features the only trampoline dodgeball court in town.",
					Image = "http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2014/08/23/59070/DSC_0212a_tip.jpg",
					ImageCaption = ""
				};
				markers.Add (attraction);
			} else if (DistanceBetween (center, cxCityLocation) < 1.0) {	//close to CX City
				//load markers close to CX City
				Attraction attraction1 = new Attraction () {
					Location = new Location() {
						Latitude = 22.315485,
						Longitude = 113.941913
					},
					AttractionType = AttractionType.Do,
					Title = "SkyCity Nine Eagles Golf Course",
					Body = "Adjacent to Terminal 2 of the Hong Kong International Airport, this is the territory’s first nine-hole golf course built to United States Golf Association standards. Test yourself over seven par-3 holes and two par-4 holes, and watch out for the signature “Island Green” hole in the middle of an artificial lake, which was inspired by the famous 17th hole at the Tournament Players Club in Sawgrass, Florida – recognised as one of golf’s toughest challenges. Travellers with time to spare will find the golf course an ideal place to check into.",
					Image = "http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/11/14/11118/Open_terrace_copy_hk_tip.jpg",
					ImageCaption = ""
				};
				markers.Add (attraction1);
				Attraction attraction2 = new Attraction () {
					Location = new Location() {
						Latitude = 22.255519,
						Longitude = 113.907861
					},
					AttractionType = AttractionType.Experience,
					Title = "Po Lin Monastery",
					Body = "Po Lin Monastery stands on the Ngong Ping plateau of Lantau Island, next to a huge bronze statue of Buddha. They attract hordes of tourists, many of whom arrive by cable car from the MTR station in Tung Chung. The popular restaurant here lets visitors eat as the monks do: only Chinese-style vegetarian dishes are served.",
					Image = "http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2013/05/13/26542/OTH-233-54928_hk_tip.jpg",
					ImageCaption = ""
				};
				markers.Add (attraction2);
				Attraction attraction3 = new Attraction () {
					Location = new Location() {
						Latitude = 22.317066,
						Longitude = 113.937496
					},
					AttractionType = AttractionType.See,
					Title = "UA IMAX Theatre",
					Body = "UA IMAX Theatre @Airport features the largest IMAX giant screen in Hong Kong and accommodates 350 audiences. The theatre is designed and positioned to maximise each audience's field of view and its projection technology possess various proprietary features including crystal-clear images, laser-aligned digital sound system as well as immersive theater design. Cinema-goers will be impressed by the world's most immersive movie experience either watching IMAX 2D or IMAX 3D movies.",
					Image = "http://cdn.assets.cougar.bauer-media.com.au/ImageGen.ashx?Image=http%3a%2f%2fassets.cougar.nineentertainment.com.au%2fassets%2fCathayPacific%2f2014%2f02%2f18%2f42748%2fUA-IMAX_Airport_wide-shoot_slideshow.jpg&Height=432&Width=784&AllowUpSizing=False&Mode=Crop",
					ImageCaption = ""
				};
				markers.Add (attraction3);
			}

			double distance = DistanceBetween (center, bmLocation);

			MapInfo mapInfo = new MapInfo () {
				Frame = new Frame() {
					Center = center,
					ZoomLevel = 14
				},
				Markers = markers
			};

			return mapInfo;
		}

		private double DistanceBetween(Location location1, Location location2)
		{
			var r = 6371;
			var dLat = ConvertToRadians(location2.Latitude - location1.Latitude);
			var dLng = ConvertToRadians(location2.Longitude - location1.Longitude);
			var lat1 = ConvertToRadians(location1.Latitude);
			var lat2 = ConvertToRadians (location2.Latitude);

			var a = Math.Sin (dLat / 2) * Math.Sin (dLat / 2) +
					Math.Sin (dLng / 2) * Math.Sin (dLng / 2) * Math.Cos (lat1) * Math.Cos (lat2);
			var c = 2 * Math.Atan2 (Math.Sqrt (a), Math.Sqrt (1 - a));
			var d = r * c;

			return d;
		}

		private double ConvertToRadians(double angle)
		{
			return (Math.PI / 180) * angle;
		}
	}
}

