﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.Services.Common
{
	public class Frame : MvxNotifyPropertyChanged
	{
		private float _zoomLevel;
		public float ZoomLevel {
			get { return _zoomLevel; }
			set {
				_zoomLevel = value;
				RaisePropertyChanged (() => ZoomLevel);
			}
		}

		private Location _center;
		public Location Center {
			get { return _center; }
			set { _center = value;
				RaisePropertyChanged (() => Center);
			}
		}

		public override string ToString ()
		{
			return string.Format ("{0} z={1}", Center, ZoomLevel);
		}
	}
}

