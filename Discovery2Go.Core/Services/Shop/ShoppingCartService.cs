﻿using System;
using System.Collections.Generic;
using Cirrious.CrossCore;


namespace Discovery2Go.Core.Services.Shop
{
	public class ShoppingCartService : IShoppingCartService
	{
		public ShoppingCartService()
		{
			_shopItems = new List<ShopItem>();
		}

		public void Add(ShopItem shopItem)
		{
			_shopItems.Add(shopItem);
		}

		public void Remove(ShopItem shopItem)
		{
			_shopItems.Remove (shopItem);
		}

		public void Clear()
		{
			_shopItems.Clear ();

		}

		public void CheckOut()
		{
			//TODO: Do nothing real here yet, implement later
			Clear();
		}

		private List<ShopItem> _shopItems;
		public List<ShopItem> ShopItems {
			get {
				return _shopItems;
			}
		}
	}
}

