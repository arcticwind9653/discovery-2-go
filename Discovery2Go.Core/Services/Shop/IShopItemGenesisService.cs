﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Shop
{
	public interface IShopItemGenesisService
	{
		List<ShopItem> GetShopItemList(string categoryName);
	}
}

