﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.Services.Shop
{
	public class ShopItem : MvxNotifyPropertyChanged
	{
		private string _productCode;
		public string ProductCode {
			get { return _productCode; }
			set {
				_productCode = value;
				RaisePropertyChanged (() => ProductCode);
			}
		}

		private string _name;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged (() => Name);
			}
		}

		private string _imageUrl;
		public string ImageUrl {
			get { return _imageUrl; }
			set {
				_imageUrl = value;
				RaisePropertyChanged (() => ImageUrl);
			}
		}

		private string _description;
		public string Description {
			get { return _description; }
			set {
				_description = value;
				RaisePropertyChanged (() => Description);
			}
		}

		private decimal _price;
		public decimal Price {
			get { return _price; }
			set {
				_price = value;
				RaisePropertyChanged (() => Price);
			}
		}
	}
}

