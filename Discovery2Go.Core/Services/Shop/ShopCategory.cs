﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.Services.Shop
{
	public class ShopCategory
	{
		public string Name { get; set; }
		public string ImageUrl { get; set; }
	}
}

