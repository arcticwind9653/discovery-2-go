﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Shop
{
	public interface IShoppingCartService
	{
		List<ShopItem> ShopItems { get; }
		void Add(ShopItem shopItem);
		void Remove(ShopItem shopItem);
		void Clear();
		void CheckOut();
	}
}

