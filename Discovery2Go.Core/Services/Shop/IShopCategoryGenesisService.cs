﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Shop
{
	public interface IShopCategoryGenesisService
	{
		List<ShopCategory> GetShopCategoryList();
	}
}

