﻿using System;
using Cirrious.CrossCore.Platform;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Shop
{
	public class ShopItemGenesisService : IShopItemGenesisService
	{
		private readonly IMvxJsonConverter _jsonConverter;

		public ShopItemGenesisService (IMvxJsonConverter jsonConverter)
		{
			_jsonConverter = jsonConverter;
		}

		public List<ShopItem> GetShopItemList (string categoryName)
		{
			List<ShopItem> shopItems;
			//TODO: mock data only. Query this from online service later
			if (categoryName == "CX Collectibles") {
				string jsonString = "[{\"productcode\":\"SFF108B\",\"name\":\"CATHAY PACIFIC SOHK Model\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001664230312101135144SFF108B-400.jpg\",\"description\":\"Hong Kong celebrated its 1997 reunion with China in many ways - \\\"The Spirit of Hong Kong\\\" was perhaps one of the most visible. The unique Boeing 747-200 was especially commissioned by Cathay Pacific to mark the handover.\",\"price\":300.0},{\"productcode\":\"SFF109B\",\"name\":\"CATHAY PACIFIC A330-300 Model 1:500\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001675230312101225847SFF109B.jpg\",\"description\":\"The die-cast metal 1:500 scale models have long been a favourite among collectors. Each one is carefully detailed and features landing gear.\",\"price\":230.0},{\"productcode\":\"SFF113A\",\"name\":\"CATHAY PACIFIC B777-200 Model 1:500\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001682230312101915704SFF113A_400.jpg\",\"description\":\"CATHAY PACIFIC B777-200 Model 1:500\",\"price\":230.0},{\"productcode\":\"SFF317M\",\"name\":\"CATHAY PACIFIC 7-in-1 Travel Companion\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000012492020414101142899SFF317ML.jpg\",\"description\":\"With its lightweight polyester material, this 7-in-1 Travel Companion holds two clothing bags, a shoe bag, a cable bag, and two underwear and sock bags, all neatly folded and packed into a toiletries bag. A great gift for travellers who like to organise their belongings properly. Size： Toiletries bag：22.5 x 14 x 9.5cm/Large clothing bag：47 x 35 x 7cm/Small clothing bag：38 x 26.5 x 7cm/Shoe bag：36.5 x 20.5 x 17cm/Cable bag：23 x 10 x 4cm/Two underwear and sock bags：31 x 23cm\",\"price\":350.0},{\"productcode\":\"SFF316B\",\"name\":\"CATHAY PACIFIC Pilot Bear Modern Edition\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000013395020414101237494SFF316BL.jpg\",\"description\":\"Delightful Pilot Bear shows off his latest navy Cathay Pacific pilot uniform. He’s looking smart and professional. He’s ready to show you around the world!\",\"price\":150.0},{\"productcode\":\"SFF108C\",\"name\":\"CATHAY PACIFIC Spirit of Hong Kong Millium\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001628230312101144217SFF108C.jpg\",\"description\":\"The Boeing 747-400 with the distinctive award winning livery that originated from the creative talents of a Hong Kong schoolgirl, \\\"The Spirit of Hong Kong Millennium Edition\\\" was commissioned to mark the start of the new millennium.\",\"price\":300.0},{\"productcode\":\"SFF109C\",\"name\":\"CATHAY PACIFIC A340-300 Model 1:500\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001677230312101234054SFF109C.jpg\",\"description\":\"The die-cast metal 1:500 scale models have long been a favourite among collectors. Each one is carefully detailed and features landing gear.\",\"price\":230.0},{\"productcode\":\"SFF113B\",\"name\":\"CATHAY PACIFIC B777-300 Model 1:500\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000001684230312101925437SFF113B.jpg\",\"description\":\"CATHAY PACIFIC B777-300 Model 1:500\",\"price\":230.0},{\"productcode\":\"SFF316E\",\"name\":\"Luggage Tag with Foldable Bag (I Fly CX)\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000015465170614152328427SFF316ELL.jpg\",\"description\":\"This lightweight polyester foldable bag is made of GPET (Green Pet) polyester, recycled from 100% quality post-consumer bottles. It comes with a cute detachable plane-shaped luggage tag that holds the folded bag and also serves as a key holder. (Bag size: 41 x 32.5cm; luggage tag size: 7.7 x 9 x 4cm)\",\"price\":75.0},{\"productcode\":\"SFF113C\",\"name\":\"CATHAY PACIFIC Boeing 747-400 Model (1:500)\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000008294230312101935011SFF113C.jpg\",\"description\":\"CATHAY PACIFIC Boeing 747-400 Model (1:500)\",\"price\":230.0},{\"productcode\":\"SFF317N\",\"name\":\"CATHAY PACIFIC B747-8F HK Trader Model 1:400\",\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/PH000012493130312152548274SFF317N.jpg\",\"description\":\"This 1:400 scale model of the Hong Kong Trader Boeing 747-8 Freighter displays a unique livery in recognition of the city’s position as one of the world’s most important trading hubs. The name Hong Kong Trader is taken from Cathay Pacific’s very first 747 freighter, which entered the fleet in 1982. The Hong Kong Trader Freighter Model must not be absent from any collection!\",\"price\":380.0}]";
				shopItems = _jsonConverter.DeserializeObject<List<ShopItem>>(jsonString);
			} else {
				shopItems = new List<ShopItem>();
				var dummyShopItem = new ShopItem() {
					ProductCode = "",
					Name = "No product in this category",
					ImageUrl = "res:icon_no_image",
					Description = "",
				};
				shopItems.Add(dummyShopItem);
			}

			return shopItems;
		}
	}
}

