﻿using System;
using System.Collections.Generic;
using Cirrious.CrossCore.Platform;

namespace Discovery2Go.Core.Services.Shop
{
	public class ShopCategoryGenesisService : IShopCategoryGenesisService
	{
		private readonly IMvxJsonConverter _jsonConverter;

		public ShopCategoryGenesisService (IMvxJsonConverter jsonConverter)
		{
			_jsonConverter = jsonConverter;
		}

		public List<ShopCategory> GetShopCategoryList ()
		{
			//TODO: mock data only, query from online service later
			string jsonString = "[{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000001300614123508880.jpg\",\"name\":\"Fragrances\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000002300614123718832.jpg\",\"name\":\"Cosmetics\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000003300614123812502.jpg\",\"name\":\"Skincare\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000006300614123858765.jpg\",\"name\":\"Watches\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000004300614124000862.jpg\",\"name\":\"Jewellery\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000005300614124043176.jpg\",\"name\":\"Electronics, Gadgets & others\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000011300614124122667.jpg\",\"name\":\"Confectionery & Spirits\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000201300614124200717.jpg\",\"name\":\"Accessories & Pens\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000202300614124236936.jpg\",\"name\":\"Children Gifts & Toys\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000008300614124335296.jpg\",\"name\":\"CX Collectibles\"},{\"imageurl\":\"http://www.cathaypacific.com/dutyfree/image/C000000161301213194744577.jpg\",\"name\":\"Fine Wines in the Air\"}]";
			var shopCategories = _jsonConverter.DeserializeObject<List<ShopCategory>> (jsonString);
			return shopCategories;
		}
	}
}

