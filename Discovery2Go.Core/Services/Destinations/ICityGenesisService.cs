﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Destinations
{
	public interface ICityGenesisService
	{
		List<City> GetCityList();
	}
}

