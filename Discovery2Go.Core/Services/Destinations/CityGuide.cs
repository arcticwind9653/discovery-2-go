﻿using System;

namespace Discovery2Go.Core.Services.Destinations
{
	public class CityGuide
	{
		public string Title { get; set; }

		public string Intro { get; set; }

		public string Image { get; set; }
	}
}

