﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Destinations
{
	public class CitySearchResult
	{
		public List<City> Cities { get; set; }
	}
}

