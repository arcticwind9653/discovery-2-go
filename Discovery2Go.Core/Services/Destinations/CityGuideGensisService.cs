﻿using System;
using System.Collections.Generic;
using Cirrious.CrossCore.Platform;

namespace Discovery2Go.Core.Services.Destinations
{
	public class CityGuideGensisService : ICityGuideGensisService
	{
		private readonly IMvxJsonConverter _jsonConverter;

		public CityGuideGensisService(IMvxJsonConverter jsonConverter)
		{
			_jsonConverter = jsonConverter;
		}

		public List<CityGuide> GetCityGuideList(string name)
		{
			List<CityGuide> cityGuides;
			//mock behaviour, return a list of guide for Tokyo only.
			if (name.ToLower () == "tokyo")
			{
				string jsonText = "[{\"image\": \"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2013/04/05/23730/116653794_home-hero.jpg\",\"intro\": \"<p><strong>FROM NEON-LIT</strong> Shibuya to neighbourly Asakusa, Tokyo is home to some of the best shopping and food in Asia – indeed, the world – and will captivate most visitors. With 36 million people in greater Tokyo, there are endless things to do and discover in this megalopolis.</p>\",\"title\": \"Discover Tokyo - Kappabashi\"},{\"image\": \"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/11/14/7577/kamakura-hero.jpg\",\"intro\": \"<p><strong>PERFECT FOR TOKYO DAY-TRIPPERS</strong>, Kamakura in Kanagawa Prefecture is just an hour away from the city. This quaint town, once the political centre of Japan, offers temples and shrines for those looking for a little history – and the beach, for those seeking surf and sunshine.</p>\",\"title\": \"Discover Tokyo - Kamakura\"},{\"image\": \"http://assets.cougar.nineentertainment.com.au/assets/CathayPacific/2012/07/29/1513/sb10066185-hero.jpg\",\"intro\": \"<p class=\\\"p1\\\"><strong>WITH A POPULATION</strong> of 36 million in its metro area, Tokyo can seem more Godzilla than city. However there are still neighbourhoods that are more human-sized and where visitors can feel less \\\"lost in translation\\\".</p>\",\"title\": \"Discover Tokyo - Koenji\"}]";
				cityGuides = _jsonConverter.DeserializeObject<List<CityGuide>>(jsonText);
			}
			else
			{
				cityGuides = new List<CityGuide>();
				var dummyCityGuide = new CityGuide() {
					Title = "No guide",
					Intro = "Opps! There's no guide for this city yet, please wait for the full version app.",
					Image = "res:icon_no_image"
				};
				cityGuides.Add(dummyCityGuide);
			}

			//TODO: query from online
			return cityGuides;
		}
	}
}

