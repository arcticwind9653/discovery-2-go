﻿using System;
using System.Collections.Generic;

namespace Discovery2Go.Core.Services.Destinations
{
	public interface ICityGuideGensisService
	{
		List<CityGuide> GetCityGuideList(string name);
	}
}

