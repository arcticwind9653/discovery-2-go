﻿using System;
using Discovery2Go.Core.Services.Common;

namespace Discovery2Go.Core.Services.Destinations
{
	public enum OperatedBy
	{
		CX = 103,
		DragonAir = 104,
		Both = 105,
	}

	public class City
	{
		public string Name { get; set; }
		public string Intro { get; set; }
		public string ImageURL { get; set; }
		public OperatedBy OperatedBy { get; set; }
		public string Country { get; set; }
		//public Location Location { get; set; }
	}
}

