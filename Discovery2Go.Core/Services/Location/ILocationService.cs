﻿using System;

namespace Discovery2Go.Core.Services.Location
{
	public interface ILocationService
	{
		//Empty interface is required for IOC injection
		void Start();
		void Stop();
	}
}

