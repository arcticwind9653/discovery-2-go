﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Discovery2Go.Core.Services.Location
{
	public class LocationMessage : MvxMessage
	{
		public LocationMessage(	object sender,
								Discovery2Go.Core.Services.Common.Location location,
								double? accuracy,
								double? heading) : base(sender)
		{
			_location = location;
			_accuracy = accuracy;
			_heading = heading;
		}

		private Discovery2Go.Core.Services.Common.Location _location;
		public Discovery2Go.Core.Services.Common.Location Location
		{
			get { return this._location; }
			set { this._location = value; }
		}

		private double? _accuracy;
		public double? Accuracy
		{
			get { return _accuracy; }
			set { _accuracy = value; }
		}

		private double? _heading;
		public double? Heading
		{
			get { return _heading; }
			set { _heading = value; }
		}

	}
}

