﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Location;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Discovery2Go.Core.Services.Location
{
	public class LocationService : ILocationService
	{
		private readonly IMvxLocationWatcher _watcher;
		private readonly IMvxMessenger _messenger;

		public LocationService(IMvxLocationWatcher watcher, IMvxMessenger messenger)
		{
			_watcher = watcher;
			_messenger = messenger;
			_watcher.Start(new MvxLocationOptions (), OnLocation, OnError);
		}

		private void OnLocation (MvxGeoLocation mvxGeoLocation)
		{
			var message =	new LocationMessage (	this,
													new Discovery2Go.Core.Services.Common.Location () {
															Latitude = mvxGeoLocation.Coordinates.Latitude,
															Longitude = mvxGeoLocation.Coordinates.Longitude,
													},
													mvxGeoLocation.Coordinates.Accuracy,
													mvxGeoLocation.Coordinates.Heading
							);

			_messenger.Publish(message);
		}

		private void OnError (MvxLocationError error)
		{
			Mvx.Error ("Location error with code {0}", error.Code);
		}

		public void Start ()
		{
			if (!_watcher.Started)
				_watcher.Start(new MvxLocationOptions (), OnLocation, OnError);
		}

		public void Stop ()
		{
			_watcher.Stop();
		}
	}
}

