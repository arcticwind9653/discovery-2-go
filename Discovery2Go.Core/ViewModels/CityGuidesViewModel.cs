﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Destinations;

namespace Discovery2Go.Core.ViewModels
{
	public class CityGuidesViewModel : BackableViewModel
	{
		public class Nav
		{
			public string Name { get; set; }
		}

		private readonly ICityGuideGensisService _cityGuideGensisService;

		public CityGuidesViewModel (ICityGuideGensisService cityGuideGensisService)
		{
			_cityGuideGensisService = cityGuideGensisService;
		}

		public void Init(Nav navigation)
		{
			CityGuides = _cityGuideGensisService.GetCityGuideList(navigation.Name);
		}

		private List<CityGuide> _cityGuides;
		public List<CityGuide> CityGuides
		{
			get { return _cityGuides; }
			set {
				_cityGuides = value;
				RaisePropertyChanged (() => CityGuides);
			}
		}

		public ICommand ShowCityGuideMapCommand
		{
			get {
				return new MvxCommand<CityGuide>(cityGuide => ShowViewModel<CityGuideMapViewModel> (new CityGuideMapViewModel.Nav () { Title = cityGuide.Title }));
			}
		}

	}
}

