﻿using System.Collections.Generic;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Destinations;

namespace Discovery2Go.Core.ViewModels
{
	public class DestinationsViewModel : BackableViewModel
	{
		public DestinationsViewModel(ICityGenesisService service)
		{
			Cities = service.GetCityList();
		}

		private List<City> _cities;
		public List<City> Cities
		{
			get { return _cities; }
			set {
				_cities = value;
				RaisePropertyChanged (() => Cities);
			}
		}

		public ICommand ShowCityGuidesCommand
		{
			get {
				return new MvxCommand<City> (city => ShowViewModel<CityGuidesViewModel> (new CityGuidesViewModel.Nav () { Name = city.Name }));
			}
		}
	}
}

