﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.Services.Destinations;
using Discovery2Go.Core.Services.Connectivity;
using Cirrious.CrossCore;

namespace Discovery2Go.Core.ViewModels
{
	public class CityGuideMapViewModel : BackableViewModel
	{
		public class Nav
		{
			public string Title { get; set; }
		}

		private readonly IMapInfoGenesisService _mapInfoGenesisService;
		private readonly IDownloadService _downloadService;

		public CityGuideMapViewModel(IMapInfoGenesisService mapInfoGenesisService, IDownloadService downloadService)
		{
			_mapInfoGenesisService = mapInfoGenesisService;
			_downloadService = downloadService;
		}

		public async void Init(Nav navigation)
		{
			MapInfo mapInfo = _mapInfoGenesisService.GetMapInfo(navigation.Title);
			if (mapInfo != null)
			{
				MapVM = new MapViewModel (_downloadService) {
					Center = mapInfo.Frame.Center,
					ZoomLevel = mapInfo.Frame.ZoomLevel,
					Markers = mapInfo.Markers
				};
				MapVM.Thumbnails = await MapVM.DownloadThumbnails(MapVM.Markers);
			}
		}

		private MapViewModel _mapVM;
		public MapViewModel MapVM{
			get { return _mapVM; }
			set {
				_mapVM = value;
				RaisePropertyChanged (() => MapVM);
			}
		}
	}
}

