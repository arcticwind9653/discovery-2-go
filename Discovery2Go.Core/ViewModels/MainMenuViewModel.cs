using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;

namespace Discovery2Go.Core.ViewModels
{
    public class MainMenuViewModel : MvxViewModel
    {

		//Button click for Nearby
		private MvxCommand _showNearbyCommand;
		public ICommand ShowNearbyCommand
		{
			get {
				_showNearbyCommand = _showNearbyCommand ?? new MvxCommand (DoShowNearby);
				return _showNearbyCommand;
			}
		}
		private void DoShowNearby()
		{
			ShowViewModel<NearbyViewModel>();
		}

		//Button click for Destination
		private MvxCommand _showDestinationsCommand;
		public ICommand ShowDestinationsCommand
		{
			get {
				_showDestinationsCommand = _showDestinationsCommand ?? new MvxCommand (DoShowDestinations);
				return _showDestinationsCommand;
			}
		}
		private void DoShowDestinations()
		{
			ShowViewModel<DestinationsViewModel>();
		}

		//Button click for Magazine
		private MvxCommand _showMagazineCommand;
		public ICommand ShowMagazineCommand
		{
			get {
				_showMagazineCommand = _showMagazineCommand ?? new MvxCommand (DoShowMagazine);
				return _showMagazineCommand;
			}
		}
		private void DoShowMagazine()
		{
			ShowViewModel<MagazineViewModel>();
		}

		//Button click for Shop
		private MvxCommand _showShopCommand;
		public ICommand ShowShopCommand
		{
			get {
				_showShopCommand = _showShopCommand ?? new MvxCommand (DoShowShop);
				return _showShopCommand;
			}
		}
		private void DoShowShop()
		{
			ShowViewModel<ShopViewModel>();
		}

//		private string _hello = "Hello MvvmCross";
//        public string Hello
//		{ 
//			get { return _hello; }
//			set { _hello = value; RaisePropertyChanged(() => Hello); }
//		}
    }
}
