﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.Services.Connectivity;

namespace Discovery2Go.Core.ViewModels
{
	public class MapViewModel : MvxViewModel
	{
		private IDownloadService _downloadService;

		public MapViewModel(IDownloadService downloadService)
		{
			_downloadService = downloadService;
		}

		private Location _center;
		public Location Center {
			get { return _center; }
			set {
				_center = value;
				RaisePropertyChanged(() => Center);
			}
		}

		private float _zoomLevel;
		public float ZoomLevel {
			get { return _zoomLevel; }
			set {
				_zoomLevel = value;
				RaisePropertyChanged(() => ZoomLevel);
			}
		}

		private List<Attraction> _markers;
		public List<Attraction> Markers {
			get { return _markers; }
			set {
				_markers = value;
				RaisePropertyChanged (() => Markers);
			}
		}

		private Dictionary<string, byte[]> _thumbnails;
		public Dictionary<string, byte[]> Thumbnails
		{
			get { return _thumbnails; }
			set {
				_thumbnails = value;
				RaisePropertyChanged (() => Thumbnails);
			}
		}

		public async Task<Dictionary<string, byte[]>> DownloadThumbnails(List<Attraction> attractions)
		{
			var thumbnails  = new Dictionary<string, byte[]>();
			foreach (Attraction attraction in attractions)
			{
				byte[] thumbnailBytes = await _downloadService.DownloadAsByteArray(attraction.Image);
				thumbnails.Add (attraction.Title, thumbnailBytes);
			}

			return thumbnails;
		}
	}

}

