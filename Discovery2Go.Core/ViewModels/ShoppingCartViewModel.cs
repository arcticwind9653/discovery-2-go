﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Shop;
using Chance.MvvmCross.Plugins.UserInteraction;

namespace Discovery2Go.Core.ViewModels
{
	public class ShoppingCartViewModel : BackableViewModel
	{
		private readonly IShoppingCartService _shoppingCartService;
		public ShoppingCartViewModel (IShoppingCartService shoppingCartService)
		{
			_shoppingCartService = shoppingCartService;
		}

		public void Init()
		{
			ItemsInCart = _shoppingCartService.ShopItems;
		}

		private List<ShopItem> _itemsInCart;
		public List<ShopItem> ItemsInCart
		{
			get { return _itemsInCart; }
			set {
				_itemsInCart = value;
				RaisePropertyChanged (() => ItemsInCart);
			}
		}

		private MvxCommand _checkoutCommand;
		public ICommand CheckoutCommand
		{
			get {
				_checkoutCommand = _checkoutCommand ?? new MvxCommand (DoCheckout);
				return _checkoutCommand;
			}
		}
		private async void DoCheckout()
		{
			decimal total = 0.0M;
			foreach (ShopItem items in ItemsInCart) {
				total += items.Price;
			}

			await Mvx.Resolve<IUserInteraction>().AlertAsync("Thank you! Total price is " + total.ToString("C2"));
			_shoppingCartService.CheckOut();
			ShowViewModel<ShopViewModel>();
		}
	}
}

