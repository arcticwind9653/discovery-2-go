﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;
using Discovery2Go.Core.Services.Shop;
using System.Windows.Input;
using Cirrious.CrossCore.Platform;

namespace Discovery2Go.Core.ViewModels
{
	public class ShopItemsViewModel : BackableViewModel
	{
		public class Nav
		{
			public string Name { get; set; }
		}

		private readonly IShopItemGenesisService _shopItemGenesisService;
		private readonly IMvxJsonConverter _jsonConverter;

		public ShopItemsViewModel (IShopItemGenesisService shopItemGenesisService, IMvxJsonConverter jsonConverter)
		{
			_shopItemGenesisService = shopItemGenesisService;
			_jsonConverter = jsonConverter;
		}

		public void Init(Nav navigation)
		{
			ShopItems = _shopItemGenesisService.GetShopItemList (navigation.Name);
		}

		private List<ShopItem> _shopItems;
		public List<ShopItem> ShopItems
		{
			get { return _shopItems; }
			set {
				_shopItems = value;
				RaisePropertyChanged (() => ShopItems);
			}
		}

		public ICommand ShowShopItemDetailCommand
		{
			get {
				return new MvxCommand<ShopItem>(shopItem => ShowViewModel<ShopItemDetailViewModel> (new ShopItemDetailViewModel.Nav () { ItemJsonString = _jsonConverter.SerializeObject(shopItem) }));
			}
		}

		private MvxCommand _showShoppingCartCommand;
		public ICommand ShowShoppingCartCommand
		{
			get {
				_showShoppingCartCommand = _showShoppingCartCommand ?? new MvxCommand (DoShowShoppingCart);
				return _showShoppingCartCommand;
			}
		}
		private void DoShowShoppingCart()
		{
			ShowViewModel<ShoppingCartViewModel>();
		}
	}
}

