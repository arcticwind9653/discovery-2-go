﻿using System;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Shop;
using Cirrious.CrossCore.Platform;
using System.Windows.Input;

namespace Discovery2Go.Core.ViewModels
{
	public class ShopItemDetailViewModel : BackableViewModel
	{
		public class Nav
		{
			public string ItemJsonString { get; set; }
		}

		private readonly IMvxJsonConverter _jsonConverter;
		private readonly IShoppingCartService _shoppingCartService;

		public ShopItemDetailViewModel(IMvxJsonConverter jsonConverter, IShoppingCartService shoppingCartService)
		{
			_jsonConverter = jsonConverter;
			_shoppingCartService = shoppingCartService;
		}

		public void Init(Nav navigation)
		{
			ShopItem = _jsonConverter.DeserializeObject<ShopItem>(navigation.ItemJsonString);
		}

		private ShopItem _shopItem;
		public ShopItem ShopItem {
			get { return _shopItem; }
			set {
				_shopItem = value;
				RaisePropertyChanged (() => ShopItem);
			}
		}

		private MvxCommand _addToCartCommand;
		public ICommand AddToCartCommand
		{
			get {
				_addToCartCommand = _addToCartCommand ?? new MvxCommand (DoAddToCart);
				return _addToCartCommand;
			}
		}
		private void DoAddToCart()
		{
			_shoppingCartService.Add(ShopItem);
			Close(this);
		}
	}
}

