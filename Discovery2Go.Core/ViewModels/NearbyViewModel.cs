﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.Services.Connectivity;
using Discovery2Go.Core.Services.Location;

namespace Discovery2Go.Core.ViewModels
{
	public class NearbyViewModel : BackableViewModel
	{
		private readonly IMapInfoGenesisService _mapInfoGenesisService;
		private readonly MvxSubscriptionToken _token;
		private readonly ILocationService _locationService;
		private readonly IDownloadService _downloadService;

		public NearbyViewModel(IMapInfoGenesisService mapInfoGenesisService, ILocationService locationService, IDownloadService downloadService, IMvxMessenger messenger)
		{
			_mapInfoGenesisService = mapInfoGenesisService;
			_token = messenger.Subscribe<LocationMessage>(OnLocationMessage);

			_locationService = locationService;
			_downloadService = downloadService;
		}

		public event EventHandler LocationFound;

		private bool _isLocationFound = false;
		public bool IsLocationFound
		{
			get { return _isLocationFound; }
		}

		private async void OnLocationMessage (LocationMessage locationMessage)
		{
			if (!_isLocationFound) {
				MapInfo mapInfo = _mapInfoGenesisService.GetMapInfo(locationMessage.Location); //(new Location (){ Latitude = 22.2980526, Longitude = 113.9341485 }); //Hard-coded CXCity location
				MapVM = new MapViewModel(_downloadService){
					Center = mapInfo.Frame.Center,
					ZoomLevel = mapInfo.Frame.ZoomLevel,
					Markers = mapInfo.Markers
				};
				MapVM.Thumbnails = await MapVM.DownloadThumbnails(MapVM.Markers);
				CurrentLocation = MapVM.Center.Clone();

				if (LocationFound != null)
					LocationFound (this, EventArgs.Empty);

				_isLocationFound = true;
			} else {
				CurrentLocation = locationMessage.Location;
			}
			Heading = locationMessage.Heading;
			Accuracy = locationMessage.Accuracy;
		}

		private MapViewModel _mapVM;
		public MapViewModel MapVM{
			get { return _mapVM; }
			set {
				_mapVM = value;
				RaisePropertyChanged (() => MapVM);
			}
		}

		private Location _currentLocation;
		public Location CurrentLocation
		{
			get { return _currentLocation; }
			set {
				_currentLocation = value;
				RaisePropertyChanged (() => CurrentLocation);
			}
		}

		private double? _heading;
		public double? Heading
		{
			get { return _heading; }
			set {
				_heading = value;
				RaisePropertyChanged (() => Heading);
			}
		}

		private double? _accuracy;
		public double? Accuracy
		{
			get { return _accuracy; }
			set {
				_accuracy = value;
				RaisePropertyChanged (() => Accuracy);
			}
		}

		public void StartLocationWatcher()
		{
			_locationService.Start();
		}

		public void StopLocationWatcher()
		{
			_locationService.Stop();
		}
	}
}

