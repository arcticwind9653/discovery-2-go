﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;

namespace  Discovery2Go.Core.ViewModels
{
	public class BackableViewModel : MvxViewModel
	{
		public BackableViewModel()
		{
			_mainMenuVM = new MainMenuViewModel();
		}

		private MvxCommand _backCommand;
		public ICommand BackCommand
		{
			get {
				_backCommand = _backCommand ?? new MvxCommand (DoBack);
				return _backCommand;
			}
		}

		private void DoBack()
		{
			if (this is NearbyViewModel || this is DestinationsViewModel || this is ShopViewModel) {			
				ShowViewModel<MainMenuViewModel> ();
			} else {
				Close (this);
			}
		}

		private MvxCommand _showMainMenuCommand;
		public ICommand ShowMainMenuCommand
		{
			get {
				_showMainMenuCommand = _showMainMenuCommand ?? new MvxCommand (DoShowMainMenu);
				return _showMainMenuCommand;
			}
		}
		private void DoShowMainMenu()
		{
			ShowViewModel<MainMenuViewModel>();
		}

		private MainMenuViewModel _mainMenuVM;
		public MainMenuViewModel MainMenuVM
		{
			get {
				return _mainMenuVM;
			}
			set {
				_mainMenuVM = value;
				RaisePropertyChanged (() => MainMenuVM);
			}
		}
	}
}

