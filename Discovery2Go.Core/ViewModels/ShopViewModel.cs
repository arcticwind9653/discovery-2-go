﻿using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Discovery2Go.Core.Services.Shop;
using System.Collections.Generic;

namespace Discovery2Go.Core.ViewModels
{
	public class ShopViewModel : BackableViewModel
	{
		public ShopViewModel(IShopCategoryGenesisService shopCategoryGenesisService)
		{
			ShopCategories = shopCategoryGenesisService.GetShopCategoryList();
		}

		private List<ShopCategory> _shopCategories;
		public List<ShopCategory> ShopCategories
		{
			get { return _shopCategories; }
			set {
				_shopCategories = value;
				RaisePropertyChanged (() => ShopCategories);
			}
		}

		public ICommand ShowShopItemsCommand
		{
			get {
				return new MvxCommand<ShopCategory> (shopCategory => ShowViewModel<ShopItemsViewModel> (new ShopItemsViewModel.Nav () { Name = shopCategory.Name }));
			}
		}

		private MvxCommand _showShoppingCartCommand;
		public ICommand ShowShoppingCartCommand
		{
			get {
				_showShoppingCartCommand = _showShoppingCartCommand ?? new MvxCommand (DoShowShoppingCart);
				return _showShoppingCartCommand;
			}
		}
		private void DoShowShoppingCart()
		{
			ShowViewModel<ShoppingCartViewModel>();
		}
	}
}

