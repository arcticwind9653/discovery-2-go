﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Discovery2Go.Core.Services.Destinations;

namespace Discovery2Go.Droid.CustomListViews
{
	public class CityListAdapter : MvxAdapter
	{
		public CityListAdapter (Context context) : base(context)
		{

		}

		protected override IMvxListItemView CreateBindableView (object dataContext, int templateId)
		{
			return new CityListItemView (Context, BindingContext.LayoutInflater, dataContext, templateId);
		}
	}
}

