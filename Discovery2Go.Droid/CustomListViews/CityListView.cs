﻿using System;
using Android.Content;
using Android.Util;
using Cirrious.MvvmCross.Binding.Droid.Views;

namespace Discovery2Go.Droid.CustomListViews
{
	public class CityListView : MvxListView
	{
		public CityListView (Context context, IAttributeSet attrs)
			: base(context, attrs, new CityListAdapter(context))
		{
		}
	}
}

