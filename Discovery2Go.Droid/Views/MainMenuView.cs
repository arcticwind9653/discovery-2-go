using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Android.Gms.Common;
using Android.Widget;
using Android.Content;

namespace Discovery2Go.Droid.Views
{
    [Activity(Label = "View for MainMenu")]
    public class MainMenuView : MvxActivity
    {
		private const int RQS_GooglePlayServices = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.MainMenuView);

			//lock screen oritentation for this page only
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
        }

		protected override void OnResume ()
		{
			base.OnResume ();

			int resultCode = GooglePlayServicesUtil.IsGooglePlayServicesAvailable (ApplicationContext);

			if (resultCode != ConnectionResult.Success) {
				GooglePlayServicesUtil.GetErrorDialog (resultCode, this, RQS_GooglePlayServices).Show();
			}
		}

		private bool exit = false;

		public override void OnBackPressed ()
		{
			if (exit) {
				Finish();
			}
			else {
				Toast.MakeText (this, "Press Back again to exit.", ToastLength.Short).Show ();
				exit = true;
				new Handler ().PostDelayed (() => {
					exit = false;
				}, 3000);
			}
		}
    }
}