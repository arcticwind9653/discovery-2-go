﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Fragging;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.ViewModels;
using Discovery2Go.Droid.Maps;

namespace Discovery2Go.Droid.Views
{
	[Activity(Label = "View for CityGuideMap")]
	public class CityGuideMapView : MvxFragmentActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.CityGuideMapView);

			var viewModel = (CityGuideMapViewModel)ViewModel;

			if (viewModel.MapVM != null) {
				var mapFragment = (MyMapFragment)SupportFragmentManager.FindFragmentById(Resource.Id.map);
				mapFragment.ViewModel = viewModel.MapVM;

				//TODO: Build a similar infowindow with no image. Used when thumbnails isn't ready
			}			
		}
	}
}

