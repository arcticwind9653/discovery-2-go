﻿using System;
using Android.OS;

namespace Discovery2Go.Droid.Views
{
	public class MyCountDownTimer : CountDownTimer
	{
		class TickEventArgs : EventArgs
		{
			public long MillisUntilFinished { get; set; }
		}

		private EventHandler _onTickHandler;
		private EventHandler _onFinishHandler;

		public MyCountDownTimer(long millisInFuture, long countdownInterval, EventHandler onTickHandler, EventHandler onFinishHandler) : base(millisInFuture, countdownInterval)
		{
			_onTickHandler = onTickHandler;
			_onFinishHandler = onFinishHandler;
		}

		public override void OnTick (long millisUntilFinished)
		{
			if (_onTickHandler != null)
			{
				_onTickHandler (this, new TickEventArgs() { MillisUntilFinished = millisUntilFinished });
			}
		}

		public override void OnFinish ()
		{
			if (_onFinishHandler != null)
				_onFinishHandler(this, EventArgs.Empty);
		}
	}
}

