﻿using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;

namespace Discovery2Go.Droid.Views
{
	[Activity(Label = "View for CityGuides")]
	public class CityGuidesView: MvxActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.CityGuidesView);
		}
	}
}

