using Android.App;
using Android.OS;
using Android.Content;
using Cirrious.MvvmCross.Droid.Views;

namespace Discovery2Go.Droid.Views
{
    [Activity(Label = "View for MagazineMenu")]
    public class MagazineView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.MagazineView);

			//WebView doesn't display the magazine at all, so we have to open the browser instead
			Android.Net.Uri uri = Android.Net.Uri.Parse ("http://discovery.cathaypacific.com/magazine/en");
			Intent intent = new Intent (Intent.ActionView);
			intent.SetData (uri);

			Intent chooser = Intent.CreateChooser (intent, "Open with");

			this.StartActivity (chooser);
            this.Finish();
        }
    }
}