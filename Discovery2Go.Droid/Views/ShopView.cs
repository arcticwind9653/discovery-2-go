using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Android.Content;

namespace Discovery2Go.Droid.Views
{
    [Activity(Label = "View for Shop")]
    public class ShopView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ShopView);
        }

		public override void OnBackPressed ()
		{
			Intent intent = new Intent (this, typeof(MainMenuView));
			intent.SetFlags (ActivityFlags.ClearTop);
			StartActivity (intent);
		}
    }
}