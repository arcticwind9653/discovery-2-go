using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Android.Graphics;
using System;
using Discovery2Go.Core.ViewModels;
using Android.Content;

namespace Discovery2Go.Droid.Views
{
    [Activity(Label = "View for Destinations")]
    public class DestinationsView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DestinationsView);
        }

		public override void OnBackPressed ()
		{
			Intent intent = new Intent (this, typeof(MainMenuView));
			intent.SetFlags (ActivityFlags.ClearTop);
			StartActivity (intent);
		}
    }
}