using System;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Droid.Fragging;
using Cirrious.MvvmCross.Droid.Views;
using Discovery2Go.Core.ViewModels;
using Discovery2Go.Droid.Converters;
using Discovery2Go.Droid.Maps;
using Android.Content;

namespace Discovery2Go.Droid.Views
{
    [Activity(Label = "View for Nearby")]
	public class NearbyView : MvxFragmentActivity
    {
		private Marker _currentPos;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.NearbyView);

			var mapFragment = (MyMapFragment)SupportFragmentManager.FindFragmentById (Resource.Id.nearbymap);
			var viewModel = (NearbyViewModel)ViewModel;
			viewModel.StartLocationWatcher();
			mapFragment.View.Visibility = Android.Views.ViewStates.Invisible;

			if (!viewModel.IsLocationFound) {
				viewModel.LocationFound += OnLocationFound;
			} else {
				OnLocationFound (this, EventArgs.Empty);
			}
        }

		protected override void OnDestroy ()
		{
			base.OnDestroy ();

			var viewModel = (NearbyViewModel)ViewModel;
			viewModel.StopLocationWatcher();
		}

		void OnLocationFound(object sender, EventArgs eventArg)
		{
			var mapFragment = (MyMapFragment)SupportFragmentManager.FindFragmentById (Resource.Id.nearbymap);
			var viewModel = (NearbyViewModel)ViewModel;
			mapFragment.ViewModel = viewModel.MapVM;

			var options = new MarkerOptions ();
			options.SetPosition (new LatLng (viewModel.CurrentLocation.Latitude, viewModel.CurrentLocation.Longitude));
			_currentPos = mapFragment.Map.AddMarker (options);
			_currentPos.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.map_person));
			var set = this.CreateBindingSet<NearbyView, NearbyViewModel> ();
			set.Bind (_currentPos).For (m => m.Position).To (vm => vm.CurrentLocation).WithConversion (new LocationToLatLngValueConverter (), null);
			set.Apply ();

			//TODO: Build a similar infowindow with no image. Used when thumbnails isn't ready

			mapFragment.View.Visibility = Android.Views.ViewStates.Visible;
		}

		public override void OnBackPressed ()
		{
			Intent intent = new Intent (this, typeof(MainMenuView));
			intent.SetFlags (ActivityFlags.ClearTop);
			StartActivity (intent);
		}
    }
}