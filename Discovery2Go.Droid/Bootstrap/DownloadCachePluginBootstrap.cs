using Cirrious.CrossCore.Plugins;

namespace Discovery2Go.Droid.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.DownloadCache.PluginLoader>
    {
    }
}