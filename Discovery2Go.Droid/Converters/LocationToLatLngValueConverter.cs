﻿using System;
using Cirrious.CrossCore.Converters;
using Discovery2Go.Core.Services.Common;
using Android.Gms.Maps.Model;

namespace Discovery2Go.Droid.Converters
{
	public class LocationToLatLngValueConverter : MvxValueConverter<Location, LatLng>
	{
		protected override LatLng Convert(Location value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return new LatLng(value.Latitude, value.Longitude);
		}

		protected override Location ConvertBack(LatLng value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return new Location() { Latitude = value.Latitude, Longitude = value.Longitude };
		}
	}
}

