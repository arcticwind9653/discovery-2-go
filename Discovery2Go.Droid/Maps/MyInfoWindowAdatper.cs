﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Fragging;
using Discovery2Go.Core.Converters;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.ViewModels;
using System.IO;

namespace Discovery2Go.Droid.Maps
{
	public class MyInfoWindowAdatper : Java.Lang.Object, GoogleMap.IInfoWindowAdapter
	{
		private readonly View mWindow;
		private readonly List<Attraction> _markers;
		private readonly Dictionary<string, byte[]> _thumbnails;
		private readonly PlainTextValueConverter converter = new PlainTextValueConverter();

		public MyInfoWindowAdatper(LayoutInflater inflater, List<Attraction> markers, Dictionary<string, byte[]> thumbnails)
		{
			mWindow = inflater.Inflate(Resource.Layout.info_window_layout, null);
			_markers = markers;
			_thumbnails = thumbnails;
		}

		public View GetInfoWindow(Marker marker)
		{
			Render(marker, mWindow);
			return mWindow;
		}

		public View GetInfoContents(Marker marker)
		{
			return null;
		}

		private void Render(Marker marker, View view)
		{
			Attraction attraction = GetAttraction(marker);

			var imageView = view.FindViewById<ImageView>(Resource.Id.thumbnail);
			string title = marker.Title;
			if (_thumbnails != null) {
				byte[] thumbnailBytes = _thumbnails [title];
				imageView.SetImageBitmap (BitmapFactory.DecodeByteArray (thumbnailBytes, 0, thumbnailBytes.Length));
			}

			TextView titleUi = ((TextView)view.FindViewById(Resource.Id.title));
			titleUi.Text = title;

			String snippet = (string)converter.Convert(attraction.Body, typeof(string), null, CultureInfo.DefaultThreadCurrentCulture);
			TextView snippetUi = (TextView)view.FindViewById(Resource.Id.body);
			snippetUi.Text = snippet;
		}

		private Attraction GetAttraction(Marker marker)
		{
			return _markers.Find(m => m.Title == marker.Title);
		}
	}
}

