﻿using System;
using Discovery2Go.Core.Services.Common;
using System.Collections.Generic;
using Android.Gms.Maps.Model;
using Android.Gms.Maps;

namespace Discovery2Go.Droid.Maps
{
	public enum MarkerStatus
	{
		Normal,
		Active,
		OutOfFocus
	}

	public class MarkerHelper
	{
		private List<Marker> _markers;
		private List<Attraction> _attractions;

		public MarkerHelper(List<Attraction> attractions)
		{
			_attractions = attractions;
		}

		public List<Marker> AddMarkers(GoogleMap map)
		{
			_markers = new List<Marker>();

			foreach (Attraction attraction in _attractions)
			{
				MarkerOptions markerOption = new MarkerOptions();
				markerOption.SetPosition(new LatLng (attraction.Location.Latitude, attraction.Location.Longitude));
				markerOption.SetTitle(attraction.Title);
				markerOption.InvokeIcon(BitmapDescriptorFactory.FromResource(GetMarkerIconResourceId(attraction.AttractionType, MarkerStatus.Normal)));

				_markers.Add(map.AddMarker(markerOption));
			}

			return _markers;
		}

		public void RestoreAllMarkers()
		{
			foreach (Marker marker in _markers) {
				ToggleHighlight(marker, MarkerStatus.Normal);
			}
		}

		public void HighlightMarker(Marker marker)
		{
			foreach (Marker otherMarker in _markers) {
				ToggleHighlight(otherMarker, MarkerStatus.OutOfFocus);
			}
			ToggleHighlight(marker, MarkerStatus.Active);
		}

		private void ToggleHighlight(Marker marker, MarkerStatus markerStatus)
		{
			AttractionType attactionType = _attractions.Find(a => a.Title == marker.Title).AttractionType;
			int markerIconId = GetMarkerIconResourceId(attactionType, markerStatus);
			marker.SetIcon(BitmapDescriptorFactory.FromResource(markerIconId));
		}

		private int GetMarkerIconResourceId(AttractionType attractionType, MarkerStatus markerStatus)
		{
			int markerIconId = 0;
			if (markerStatus == MarkerStatus.Normal) {
				switch (attractionType) {
				case AttractionType.Do:
				case AttractionType.Experience:
				case AttractionType.See:
				case AttractionType.Walk:
					markerIconId = Resource.Drawable.map_marker_normal_explore;
					break;
				case AttractionType.Drink:
					markerIconId = Resource.Drawable.map_marker_normal_drink;
					break;
				case AttractionType.Eat:
					markerIconId = Resource.Drawable.map_marker_normal_eat;
					break;
				case AttractionType.Shop:
					markerIconId = Resource.Drawable.map_marker_normal_shop;
					break;
				case AttractionType.Stay:
					markerIconId = Resource.Drawable.map_marker_normal_stay;
					break;
				}
			} else if (markerStatus == MarkerStatus.Active) {
				switch (attractionType) {
				case AttractionType.Do:
				case AttractionType.Experience:
				case AttractionType.See:
				case AttractionType.Walk:
					markerIconId = Resource.Drawable.map_marker_active_explore;
					break;
				case AttractionType.Drink:
					markerIconId = Resource.Drawable.map_marker_active_drink;
					break;
				case AttractionType.Eat:
					markerIconId = Resource.Drawable.map_marker_active_eat;
					break;
				case AttractionType.Shop:
					markerIconId = Resource.Drawable.map_marker_active_shop;
					break;
				case AttractionType.Stay:
					markerIconId = Resource.Drawable.map_marker_active_stay;
					break;
				}
			} else if (markerStatus == MarkerStatus.OutOfFocus) {
				switch (attractionType) {
				case AttractionType.Do:
				case AttractionType.Experience:
				case AttractionType.See:
				case AttractionType.Walk:
					markerIconId = Resource.Drawable.map_marker_oof_explore;
					break;
				case AttractionType.Drink:
					markerIconId = Resource.Drawable.map_marker_oof_drink;
					break;
				case AttractionType.Eat:
					markerIconId = Resource.Drawable.map_marker_oof_eat;
					break;
				case AttractionType.Shop:
					markerIconId = Resource.Drawable.map_marker_oof_shop;
					break;
				case AttractionType.Stay:
					markerIconId = Resource.Drawable.map_marker_oof_stay;
					break;
			}
		}
			return markerIconId;
		}
	}
}

