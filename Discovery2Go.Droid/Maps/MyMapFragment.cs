﻿using System;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Views;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Droid.Fragging.Fragments;
using Discovery2Go.Core.Services.Common;
using Discovery2Go.Core.ViewModels;
using Discovery2Go.Droid.Converters;
using System.Collections.Generic;

namespace Discovery2Go.Droid.Maps
{
	public class MyMapFragment
		: MvxMapFragment
	{
		private bool _bound;

		private LayoutInflater _inflater;

		public override Android.Views.View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			_inflater = inflater;
			var toReturn = base.OnCreateView(_inflater, container, savedInstanceState);

			CreateBinding(_inflater);

			return toReturn;
		}

		public event EventHandler CenterChanged;
		private LatLng _center;
		public LatLng Center{
			get { return _center; }
			set	{
				_center = value;    
				var center = CameraUpdateFactory.NewLatLngZoom(value, ZoomLevel);
				Map.MoveCamera(center);
			}
		}

		public event EventHandler ZoomLevelChanged;
		private float _zoomLevel;
		public float ZoomLevel{
			get { return _zoomLevel; }
			set {
				_zoomLevel = value;
				var zoom = CameraUpdateFactory.NewLatLngZoom(Center, value);
				Map.MoveCamera(zoom);
			}
		}

		public event EventHandler MarkersChanged;
		private List<Attraction> _markers;
		public List<Attraction> Markers {
			get { return _markers; }
			set {
				_markers = value;
			}
		}

		public event EventHandler ThumbnailsChanged;
		private Dictionary<string, byte[]> _thumbnails;
		public Dictionary<string, byte[]> Thumbnails
		{
			get { return _thumbnails; }
			set {
				_thumbnails = value;

				MarkerHelper markerHelper = new MarkerHelper(Markers);
				markerHelper.AddMarkers (Map);
				Map.MarkerClick += (sender, e) => {
					if (!string.IsNullOrEmpty(e.Marker.Title)) //disable Marker click with no title, which is the current location marker
					{
						var moveToMarker = CameraUpdateFactory.NewLatLng(e.Marker.Position);
						Map.AnimateCamera(moveToMarker);

						markerHelper.HighlightMarker(e.Marker);

						e.Marker.ShowInfoWindow();
					}
				};
				Map.SetInfoWindowAdapter(new MyInfoWindowAdatper(_inflater, Markers, Thumbnails));
				Map.InfoWindowClick += (sender, e) => {
					e.Marker.HideInfoWindow();
					markerHelper.RestoreAllMarkers();
				};
				Map.MapClick += (sender, e) => {
					markerHelper.RestoreAllMarkers();
				};
			}
		}


		private void CreateBinding(LayoutInflater inflater)
		{
			if (_bound)
				return;

			MarkersChanged.Raise(this);
			ThumbnailsChanged.Raise(this);

			Map.CameraChange += MapOnCameraChange;
			this.EnsureBindingContextIsSet(inflater);

			this.DelayBind(() =>
				{
					var set = this.CreateBindingSet<MyMapFragment, MapViewModel>();
					set.Bind(this).For(v => v.Center).To(vm => vm.Center).WithConversion(new LocationToLatLngValueConverter(), null);
					set.Bind(this).For(v => v.ZoomLevel).To(vm => vm.ZoomLevel);
					set.Bind(this).For(v => v.Markers).To(vm => vm.Markers);
					set.Bind(this).For(v => v.Thumbnails).To(vm => vm.Thumbnails);
					set.Apply();
				});

			_bound = true;
		}

		private void MapOnCameraChange(object sender, GoogleMap.CameraChangeEventArgs cameraChangeEventArgs)
		{
			_center = cameraChangeEventArgs.Position.Target;
			_zoomLevel = cameraChangeEventArgs.Position.Zoom;
			CenterChanged.Raise (this);
			ZoomLevelChanged.Raise (this);
			MarkersChanged.Raise (this);
		}
	}
}

